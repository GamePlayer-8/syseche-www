#!/bin/sh

FORGE_DOMAIN="$(echo "$CI_REPO_URL" | cut -d '/' -f 3)"

if ! [ "$FORGE_DOMAIN" = "codeberg.org" ]; then
    exit 0
fi

SCRIPT_PATH="$(dirname "$(realpath "$0")")"

cd "$SCRIPT_PATH/config"

echo '<!DOCTYPE html>' > index.html
echo '<html>' >> index.html
cat "$SCRIPT_PATH/res/head.html" >> index.html

echo '<body>' >> index.html
echo '<div class="content" id="content">' >> index.html
echo '<ul>' >> index.html
for html_file in $(ls -a); do
    echo '<li><a href="'"$html_file"'">Show '"$html_file"'</a></li>' >> index.html
done
echo '</ul>' >> index.html
echo '</div>' >> index.html
echo '</body>' >> index.html
echo '</html>' >> index.html

cd nginx

echo '<!DOCTYPE html>' > index.html
echo '<html>' >> index.html
cat "$SCRIPT_PATH/res/head.html" >> index.html

echo '<body>' >> index.html
echo '<div class="content" id="content">' >> index.html
echo '<ul>' >> index.html
for html_file in $(ls -a); do
    echo '<li><a href="'"$html_file"'">Show '"$html_file"'</a></li>' >> index.html
done
echo '</ul>' >> index.html
echo '</div>' >> index.html
echo '</body>' >> index.html
echo '</html>' >> index.html

cd "$SCRIPT_PATH/html"

echo '<!DOCTYPE html>' > index.html
echo '<html>' >> index.html
cat "$SCRIPT_PATH/res/head.html" >> index.html

echo '<body>' >> index.html
echo '<div class="content" id="content">' >> index.html
echo '<ul>' >> index.html
for html_file in $(ls codes/*.html | cut -f 2 -d '/'); do
    echo '<li><a href="'"$html_file"'">Show '"$html_file"'</a></li>' >> index.html
done
echo '</ul>' >> index.html
echo '</div>' >> index.html
echo '</body>' >> index.html
echo '</html>' >> index.html

mv index.html codes/

echo '<!DOCTYPE html>' > index.html
echo '<html>' >> index.html
cat "$SCRIPT_PATH/res/head.html" >> index.html

echo '<body>' >> index.html
echo '<div class="content" id="content">' >> index.html
markdown "$SCRIPT_PATH/README.md" >> index.html
echo '</div>' >> index.html
echo '</body>' >> index.html
echo '</html>' >> index.html

cp "$SCRIPT_PATH/README.md" "$SCRIPT_PATH/html/"
cp "$SCRIPT_PATH/.woodpecker.yml" "$SCRIPT_PATH/html/"
cp "$SCRIPT_PATH/config" "$SCRIPT_PATH/html/" -r
