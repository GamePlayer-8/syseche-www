# syseche-www

Configurations files for nginx software.

# Discover available error codes

Available error codes are listed under [codes/](codes/).

# Installation

## NGINX

NGINX configs are available under [config/nginx](config/nginx) directory.
It is prepared for Let's Encrypt.

# License

[GPL 3.0](LICENSE.txt)

# Source code

The source code is available at [Codeberg](https://codeberg.org/GamePlayer-8/syseche-www).
